/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <stdio.h>

int main(void)
{
	int i = 0;

	while (1) {
		printf("Hello World #%d! %s\n", i, CONFIG_BOARD);
		k_sleep(K_MSEC(1000));
		i++;
	}

	return 0;
}

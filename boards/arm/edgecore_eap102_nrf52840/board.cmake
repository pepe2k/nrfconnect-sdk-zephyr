# SPDX-License-Identifier: Apache-2.0

board_runner_args(jlink "--device=nRF52840_xxAA" "--speed=4000")
board_runner_args(pyocd "--target=nrf52840" "--frequency=4000000")
include(${ZEPHYR_BASE}/boards/common/jlink.board.cmake)
include(${ZEPHYR_BASE}/boards/common/pyocd.board.cmake)

# TODO: only for adapter testing
set(OPENOCD_NRF5_INTERFACE "ftdi/jtag-lock-pick_tiny_2")
include(${ZEPHYR_BASE}/boards/common/openocd-nrf5.board.cmake)

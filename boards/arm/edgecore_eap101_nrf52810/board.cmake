# SPDX-License-Identifier: Apache-2.0

# TODO: testing adapters which are 1V8 compliant
set(OPENOCD_NRF5_INTERFACE "ftdi/jtag-lock-pick_tiny_2")
include(${ZEPHYR_BASE}/boards/common/openocd-nrf5.board.cmake)

# SPDX-License-Identifier: Apache-2.0

# TODO: only for adapter testing
set(OPENOCD_NRF5_INTERFACE "ftdi/jtag-lock-pick_tiny_2")
include(${ZEPHYR_BASE}/boards/common/openocd-nrf5.board.cmake)

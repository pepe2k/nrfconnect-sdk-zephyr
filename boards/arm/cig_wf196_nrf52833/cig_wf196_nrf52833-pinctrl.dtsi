/*
 * SPDX-License-Identifier: Apache-2.0
 */

/*
 * Connection between host SoC (Qualcomm IPQ8072A) and radio (nRF52833):
 *
 * IPQ8072A pin/func/dir		<>	nRF52833 pin/func/dir
 * =====================================================================
 * GPIO34  GPIO_IN_OUT    OUT		->	P0.18  nRESET   IN
 * GPIO48  BLSP2_UART_RX  IN		<-	P0.06  UART_TX  OUT
 * GPIO49  BLSP2_UART_TX  OUT		->	P0.08  UART_RX  IN
 *
 */

&pinctrl {
	uart0_default: uart0_default {
		group1 {
			psels = <NRF_PSEL(UART_TX, 0, 6)>;
		};

		group2 {
			psels = <NRF_PSEL(UART_RX, 0, 8)>;
			bias-pull-up;
		};
	};

	uart0_sleep: uart0_sleep {
		group1 {
			psels = <NRF_PSEL(UART_RX, 0, 8)>,
				<NRF_PSEL(UART_TX, 0, 6)>;
			low-power-enable;
		};
	};
};
